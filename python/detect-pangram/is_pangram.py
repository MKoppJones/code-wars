import codewars_test as Test
import string

def is_pangram(s):
    letters = string.ascii_letters[:26]
    return len([letter for letter in letters if letter.isalpha() and letter not in s.lower()]) == 0

pangram = "ABCD45EFGH,IJK,LMNOPQR56STUVW3XYZ"
Test.assert_equals(is_pangram(pangram), True)
