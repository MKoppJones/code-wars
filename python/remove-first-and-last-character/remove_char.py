import codewars_test as Test

def remove_char(s):
    return s[1:len(s)-1]


Test.describe('is_triangle')

Test.it("works for some examples")
Test.describe("Tests")
Test.assert_equals(remove_char('eloquent'), 'loquen')
Test.assert_equals(remove_char('country'), 'ountr')
Test.assert_equals(remove_char('person'), 'erso')
Test.assert_equals(remove_char('place'), 'lac')
Test.assert_equals(remove_char('ok'), '')
