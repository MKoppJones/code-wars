import codewars_test as Test

def calculate_years(principal, interest, tax, desired):
    years = 0
    while principal < desired:
        accrued = principal * interest
        principal += accrued
        principal -= accrued * tax
        years += 1
    return years

Test.describe("calculate_years")

Test.it("works for some examples")
Test.assert_equals(calculate_years(1000, 0.05, 0.18, 1100), 3)
Test.assert_equals(calculate_years(1000,0.01625,0.18,1200), 14)
Test.assert_equals(calculate_years(1000,0.05,0.18,1000), 0)
