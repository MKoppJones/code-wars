import codewars_test as test

def unique_in_order(iterable):
    last = None
    out = []
    for x in iterable:
      if last != x:
        out.append(x)
        last = x
    return out

test.assert_equals(unique_in_order('AAAABBBCCDAABBB'), ['A','B','C','D','A','B'])
