import codewars_test as test

def dirReduc(arr):
    sample = []
    index = 0
    changes = False
    for step in arr[:]:
        sample.append(step.lower())
        if len(sample) > 2:
            sample.pop(0)
        if len(sample) == 2:
            ns = set(sample) == set({ 'north', 'south' })
            ew = set(sample) == set({ 'east', 'west' })
            if ns or ew:
                arr.pop(index)
                index -= 1
                arr.pop(index)
                index -= 1
                changes = True
                sample.clear()
        index += 1
    if changes:
        return dirReduc(arr)
    return arr
    
# a = ["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]
# test.assert_equals(dirReduc(a), ['WEST'])
# u=["NORTH", "WEST", "SOUTH", "EAST"]
# test.assert_equals(dirReduc(u), ["NORTH", "WEST", "SOUTH", "EAST"])
u=['NORTH', 'SOUTH', 'EAST', 'WEST', 'NORTH', 'NORTH', 'SOUTH', 'NORTH', 'WEST', 'EAST']
test.assert_equals(dirReduc(u), ["NORTH", "NORTH"])