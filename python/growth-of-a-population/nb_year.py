import codewars_test as Test

def nb_year(p0, percent, aug, p):
    years = 0

    percent /= 100

    while p0 < p:
        p0 = p0 + (p0 * percent) + aug
        years += 1

    return years

Test.describe("nb_year")
Test.it("Basic tests")
Test.assert_equals(nb_year(1000, 2, 50, 1200), 3)
Test.assert_equals(nb_year(1500, 5, 100, 5000), 15)
Test.assert_equals(nb_year(1500000, 2.5, 10000, 2000000), 10)
