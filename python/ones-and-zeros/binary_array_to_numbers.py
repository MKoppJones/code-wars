import codewars_test as Test

def binary_array_to_number(arr):
  arr.reverse()
  tot = 0
  i = 1
  for x in arr:
    tot += (x * i)
    i *= 2
  return tot

Test.describe("One's and Zero's")
Test.it("Example tests")
Test.assert_equals(binary_array_to_number([0,0,0,1]), 1)
Test.assert_equals(binary_array_to_number([0,0,1,0]), 2)
Test.assert_equals(binary_array_to_number([1,1,1,1]), 15)
Test.assert_equals(binary_array_to_number([0,1,1,0]), 6)
