def isValidWalk(walk):
    #determine if walk is valid
    if len(walk) != 10:
        return False
    
    steps = {
        'n': 0,
        's': 0,
        'e': 0,
        'w': 0
    }
    
    for step in walk:
        steps[step] = steps[step] + 1
    
    if steps['n'] == steps['s'] and steps['e'] == steps['w']:
        return True
    return False
