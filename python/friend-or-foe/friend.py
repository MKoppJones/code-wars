import codewars_test as Test

def friend(x):
  return [name for name in x if len(name) == 4]

Test.assert_equals(friend(["Ryan", "Kieran", "Mark",]), ["Ryan", "Mark"])
