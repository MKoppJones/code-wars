import codewars_test as Test

def sort_array(source_array):
    if len(source_array) == 0:
        return source_array
    
    evens = {}
    i = 0

    for x in source_array[:]:
        # If even, store with index and remove from source
        if x % 2 == 0:
            evens[str(x)] = i
            source_array.remove(x)
        i += 1

    # Sort the remaining odd values
    source_array.sort()

    # Add evens back in
    for key in evens:
        source_array.insert(evens[key], int(key))
        
    return source_array


Test.assert_equals(sort_array([5, 3, 2, 8, 1, 4]), [1, 3, 2, 8, 5, 4])
Test.assert_equals(sort_array([5, 3, 1, 8, 0]), [1, 3, 5, 8, 0])
Test.assert_equals(sort_array([]),[])
