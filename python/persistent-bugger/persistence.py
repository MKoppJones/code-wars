import codewars_test as Test

def persistence(n):
    return calc(0, n)

def calc(step, value):
    if len(str(value)) == 1:
        return step
    new_value = 1
    for x in str(value):
        new_value *= int(x)
        
    return calc(step + 1, new_value)

Test.it("Basic tests")
Test.assert_equals(persistence(39), 3)
Test.assert_equals(persistence(4), 0)
Test.assert_equals(persistence(25), 2)
Test.assert_equals(persistence(999), 4)
