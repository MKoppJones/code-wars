import codewars_test as test

def dig_pow(n, p):
    digits = str(n)
    tot = 0
    i = 0
    for x in digits:
      tot = tot + pow(int(x), p + i)
      i = i + 1
    k = tot/n
    return k if k.is_integer() else -1

test.assert_equals(dig_pow(89, 1), 1)
test.assert_equals(dig_pow(92, 1), -1)
test.assert_equals(dig_pow(46288, 3), 51)
