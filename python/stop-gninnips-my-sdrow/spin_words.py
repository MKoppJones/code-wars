import codewars_test as test

def spin_words(sentence):
    words = sentence.split()
    index = 0
    for word in words[:]:
        if len(word) >= 5:
            words[index] = word[::-1]
        index += 1
    return ' '.join(words)

test.assert_equals(spin_words("Welcome"), "emocleW")
